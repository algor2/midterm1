/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.algormidterm1;

import java.util.Scanner;

/**
 *
 * @author Aritsu
 */
public class Midterm1 {
    
    
    static void reverseArray(int n, String arr[]){
        
        
        System.out.println("Array before reverse : ");
        for(String a : arr){
            System.out.print(a+" ");
        }
        
        for(int i = 0 ; i < n/2 ; i++){ // จับคู่ สลับตัวหน้าหลัง
            String temp = arr[i];
            arr[i] = arr[n-i-1];
            arr[n-i-1] = temp;
        }
        System.out.println();
        
        System.out.println("Array after reverse : ");
        for(String b : arr){
            System.out.print(b+ " ");
        }
        
    }
    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Input length of array : ");
        int n = s.nextInt();
        
        String[] arr = new String[n];
        
        System.out.println("Input element in array : ");
        for(int i = 0 ; i < n ; i++){
            arr[i] = s.next();
        }
        
        reverseArray(n,arr );
    }
}
